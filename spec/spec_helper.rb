# frozen_string_literal: true

require 'bundler/setup'
Bundler.setup

require 'simplecov'
SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new([
  SimpleCov::Formatter::HTMLFormatter
])
SimpleCov.start { add_filter '/spec/' }

require 'kramdown'
require 'gitlab_kramdown'

# Load support files
Dir[File.join('./spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  # some (optional) config here
end
